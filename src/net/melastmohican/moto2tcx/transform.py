'''
Created on May 3, 2014

@author: mordor
'''

import csv
import sys

from lxml import etree as ET
from lxml.builder import ElementMaker


class Transform(object):
    '''
    classdocs
    '''
    activities = {1 : 'Running', 2 : 'Running', 4 : 'Biking'}


    def __init__(self, path):
        '''
        Constructor
        '''
        self.path = path
        
    def tranfrom(self):
        csv = self.readCSV() 
        
    def Activity(self, activity):
        try:
            return self.activities[activity]
        except:
            return "Other"
        
    def Trackpoints(self, lines):
        elements = []
        for line in lines:
            E = ElementMaker()
            element = E.Trackpoint()
            elements.append(element)
        return elements
    
        
    def readCSV(self):
        try:
            with open(self.path) as csvFile:
                lines = csv.DictReader(csvFile, delimiter=',')
                
                rows = list(lines)
                size = len(rows)
                firstLine = rows[0]
                lastLine = rows[size - 1]
                sumHR = sum(int(float(row['HEARTRATE'])) for row in rows)
                avgHR = float(sumHR / size)
                
                maxHR = max(int(float(row['HEARTRATE'])) for row in rows)
                maxSpeed = max(int(float(row['SPEED'])) for row in rows)
                start = long(firstLine['timestamp_epoch'])
                end = long(lastLine['timestamp_epoch'])
                total = (end - start) / 1000
                distance = float(lastLine['DISTANCE'])
                calories = int(float(lastLine['CALORIEBURN']))
                sport = self.Activity(int(firstLine['activity_id']))
               
                garmin = "http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2"
                xsi = "http://www.w3.org/2001/XMLSchema-instance"
                schemaLocation = "http://www.garmin.com/xmlschemas/ActivityExtension/v2 http://www.garmin.com/xmlschemas/ActivityExtensionv2.xsd http://www.garmin.com/xmlschemas/FatCalories/v1 http://www.garmin.com/xmlschemas/fatcalorieextensionv1.xsd http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2 http://www.garmin.com/xmlschemas/TrainingCenterDatabasev2.xsd"
                E = ElementMaker(nsmap={None: garmin, 'xsi': xsi})                                 
                doc = E.TrainingCenterDatabase({"{" + xsi + "}schemaLocation" : schemaLocation},
                    E.Folders(),
                    E.Activities(
                        E.Activity({"Sport":sport},
                            E.Id(# ISO8601UTC.format(new Date(start))
                                 ),
                            E.Lap(
                              # StartTime:ISO8601UTC.format(start)
                              E.TotalTimeSeconds(str(total)),
                              E.DistanceMeters(str(distance)),
                              E.MaximumSpeed(str(maxSpeed)),
                              E.Calories(str(calories)),
                              E.AverageHeartRateBpm(E.Value(str(avgHR))),
                              E.MaximumHeartRateBpm(E.Value(str(maxHR))),
                              E.Intensity("Active"),
                              E.TriggerMethod("Distance"),
                              E.Track(*self.Trackpoints(lines)),
                            )
                        )
                    ) 
                )
                print(ET.tostring(doc, pretty_print=True, xml_declaration=True, encoding='utf-8'))   
        
                
        except Exception, e:
            print "Unexpected error:", repr(e)
        
        
