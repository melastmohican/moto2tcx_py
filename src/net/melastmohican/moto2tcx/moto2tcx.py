#!/usr/bin/python
'''
net.melastmohican.moto2tcx.moto2tcx -- Converts MOTOACTV workout files to Garmin TCX format

net.melastmohican.moto2tcx.main is a description

It defines classes_and_methods

@author:     melastmohican

@copyright:  2014 Mariusz S. Jurgielewicz. All rights reserved.

@contact:    mordor@mail.com

@deffield    updated: Updated
'''

from argparse import ArgumentParser
import os
import sys

from net.melastmohican.moto2tcx.transform import Transform


__all__ = []
__version__ = 0.1
__date__ = '2014-05-01'
__updated__ = '2014-05-01'

def diag():
    print("Path at terminal when executing this file")
    print(os.getcwd() + "\n")

    print("This file path, relative to os.getcwd()")
    print(__file__ + "\n")
    
    print("This file full path (following symlinks)")
    full_path = os.path.realpath(__file__)
    print(full_path + "\n")
    
    print("This file directory and name")
    path, file = os.path.split(full_path)
    print(path + ' --> ' + file + "\n")
    
    print("This file directory only")
    print(os.path.dirname(full_path))

def main(argv=None): # IGNORE:C0111
    '''Command line options.'''

    if argv is None:
        argv = sys.argv
    else:
        sys.argv.extend(argv)

    #diag()
    
    program_name = os.path.basename(sys.argv[0])
    program_version = "v%s" % __version__
    program_build_date = str(__updated__)
    program_version_message = '%%(prog)s %s (%s)' % (program_version, program_build_date)

    try:
        # Setup argument parser
        parser = ArgumentParser()
        parser.add_argument('-V', '--version', action='version', version=program_version_message)
        parser.add_argument(dest="paths", help="paths to folder(s) with source file(s) [default: %(default)s]", metavar="path", nargs='+')

        # Process arguments
        args = parser.parse_args()

        paths = args.paths
        for inpath in paths:
            ### do something with inpath ###
            print(inpath)
            trans = Transform(inpath)
            trans.tranfrom()
        return 0
    except KeyboardInterrupt:
        ### handle keyboard interrupt ###
        return 0
    except Exception, e:
        indent = len(program_name) * " "
        sys.stderr.write(program_name + ": " + repr(e) + "\n")
        sys.stderr.write(indent + "  for help use --help")
        return 2

if __name__ == "__main__":
    sys.exit(main())